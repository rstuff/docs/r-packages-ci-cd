# R Package Template fr CI/CD piepline.
# Copyright (C) 2016 Jean-François Rey <jean-françois.rey@inra.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#' An awesome method that return if the parameter is TRUE
#' @title Test parameter is TRUE
#' @name awesomemethod
#' @param bool a boolean (default TRUE)
#' @return TRUE if bool is TRUE otherwise FALSE
#' @include packagertemplate.R
#' @export
awesomemethod <- function(bool = TRUE) {
  return(bool == TRUE)
}
